/* eslint no-console:0 import/no-extraneous-dependencies:0 */
const config = require('./webpack.config');
const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

config.entry.unshift(
  'webpack-dev-server/client?http://localhost:5000/',
  'webpack/hot/dev-server'
);

config.plugins.unshift(
  new webpack.HotModuleReplacementPlugin()
);

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  contentBase: 'www',
  hot: true,
  noInfo: true,
  stats: { colors: true },
}).listen(5000, 'localhost', (err) => {
  if (err) console.error('[webpack-dev-server] Error:', err);
  console.log('[webpack-dev-server] Listening at localhost:5000');
});
