import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

function App(props) {
  return (
    <div>
      <Link to="/login">Login</Link>
      <h2>Hello App Component</h2>
      { JSON.stringify(props.state) }
    </div>
  );
}

App.propTypes = {
  state: PropTypes.object
};

function mapStateToProps(state) {
  return {
    state
  };
}

export default connect(mapStateToProps)(App);
