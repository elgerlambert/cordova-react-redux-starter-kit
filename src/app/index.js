import React from 'react';
import ReactDOM from 'react-dom';
import { hashHistory } from 'react-router';
import configureStore from './store/configureStore';

const store = configureStore();
const rootEl = document.getElementById('root');

const render = () => {
  const Root = require('./containers/Root').default; // eslint-disable-line global-require
  ReactDOM.render(<Root store={store} history={hashHistory} />, rootEl);
};

if (module.hot) {
  module.hot.accept('./containers/Root', () => {
    render();
  });
}

render();
