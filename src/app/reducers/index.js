import { combineReducers } from 'redux';

function counter(state = 0) {
  return state + 5;
}

export default combineReducers({
  counter
});
