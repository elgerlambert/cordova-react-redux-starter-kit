const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const args = process.argv.slice(2);
const __PRODUCTION__ = args.indexOf('-p') !== -1;

module.exports = {
  entry: [
    './src/app/index.js'
  ],
  debug: !__PRODUCTION__,
  devtool: __PRODUCTION__ ? false : 'eval',
  output: {
    path: path.join(__dirname, 'www'),
    publicPath: '',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel' }
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      inject: 'body'
    })
  ].concat(__PRODUCTION__ ? [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    })
  ] : [])
};
